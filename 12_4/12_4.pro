TEMPLATE = app
#TARGET = videowidget
CONFIG += console

#HEADERS += src/videoplayer.h \


SOURCES += src/ch12_ex12_4.cpp 

	
INCLUDEPATH += /usr/local/include\
               /usr/local/include/opencv\
               /usr/local/include/opencv2


LIBS += /usr/local/lib/libopencv_highgui.so\
        /usr/local/lib/libopencv_core.so\
        /usr/local/lib/libopencv_imgproc.so\
        /usr/local/lib/libopencv_features2d.so\
        /usr/local/lib/libopencv_video.so

#RESOURCES += videowidget.qrc
#INSTALLS += target

#QT+=widgets
